package com.makingdevs;

import org.junit.*;
import static org.junit.Assert.*;

class PasswordVerifierTest {

  PasswordVerifier verifier = new PasswordVerifier();

  @Test
  void atLeastOneUppercaseLetter(){
    assert !verifier.isAtLeastOneUppercaseLetter("comandodos");
    assert verifier.isAtLeastOneUppercaseLetter("comandodoS");
    assert verifier.isAtLeastOneUppercaseLetter("Comandodo");
    assert verifier.isAtLeastOneUppercaseLetter("COMANDODOS");
  }

  @Test
  void passwordShouldBeLargerThan8Test(){
    /*assert(verifier.verify("12345") == false);
    assert(verifier.verify("1234567") == false);
    assert(verifier.verify("12345678") == true);
    assert(verifier.verify("123456789") == true);*/
	assert verifier.isPasswordShouldBeLargerThan8("824697315");
  }

  @Test
  void consecutiveNumbersTest() {
    assert verifier.isConsecutiveNumbers("password")
    assert verifier.isConsecutiveNumbers("1password")
    assert verifier.isConsecutiveNumbers("password1")
    assert verifier.isConsecutiveNumbers("pass1word")
    assert verifier.isConsecutiveNumbers("1pass2word3")
    assert verifier.isConsecutiveNumbers("1p2a3ssword")
    assert !verifier.isConsecutiveNumbers("123password")
    assert !verifier.isConsecutiveNumbers("pass123word")
    assert !verifier.isConsecutiveNumbers("password123")
  }

  @Test
  void notNullTest() {
    assert !verifier.isNotNull(null)
    assert verifier.isNotNull("algo")
  }

  @Test
  void specialCharsRequested() {
    assert verifier.isSpecialCharsRequested('jlnjkljkl232') == false
    assert verifier.isSpecialCharsRequested('hcdjkhjlk&%%') == true
  }

}
