package com.makingdevs

import spock.lang.*

class PasswordVerifierSpec extends Specification {

  @Unroll("The password #password is valid? #_isValid")
  def "The password should be valid"(){
    given:
      PasswordVerifier verifier = new PasswordVerifier()
    when:
        boolean isValid = verifier.verify password
    then:
        isValid == _isValid
    where:
    password     || _isValid
    "comandodos" || false
    "comandodoS" || false
    "Comandodo" || false
    "COMANDODOS" || false
    "12345" || false
    "1234567" || false
    "12345678"  || false
    "123456789"  || false 
    "password"  || false
    "1password"       || false
    "password1"       || false
    "pass1word"       || false
    "1pass2word3"     || false
    "1p2a3ssword"    || false
    "123password"    || false
    "pass123word"    || false
    "password123"    || false
    //null  || false
    "algo" || false
    "jlnjkljkl232" || false
    "hcdjkhjlk&%%" || false
  }
}
