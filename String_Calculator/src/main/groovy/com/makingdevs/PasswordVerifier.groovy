package com.makingdevs;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PasswordVerifier {
  public boolean verify(String password) {
    isAtLeastOneUppercaseLetter(password) &&
            isPasswordShouldBeLargerThan8(password) &&
            isConsecutiveNumbers(password) &&
            isNotNull(password) &&
            isSpecialCharsRequested(password);
  }

  public boolean isAtLeastOneUppercaseLetter(String password) throws NullPointerException {
    !password.toLowerCase().equals(password)
  }

  public boolean isPasswordShouldBeLargerThan8(String password){
    password.length() > 8
  }

 public boolean isConsecutiveNumbers(String password){
   def matcher = password =~ /.*(?:012|210|123|321|234|432|345|543|456|654|567|765|678|876|789|987).*/
   !matcher.matches()
 }

  public boolean isNotNull(String password){
    password ? true : false
  }

  public boolean isSpecialCharsRequested( String password ) {
    if ( !password ) {
      false
    } else {
      Pattern p = Pattern.compile("[^A-Za-z0-9]")
      Matcher m = p.matcher( password )
      boolean b = m.find()
      b ? true : false
    }
  }

}

